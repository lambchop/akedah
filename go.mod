module skeleton.army/akedah

go 1.17

require (
	github.com/hajimehoshi/ebiten v1.12.12
	github.com/ungerik/go3d v0.0.0-20210728072715-20a26e3f077d
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200707082815-5321531c36a2 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/mobile v0.0.0-20210208171126-f462b3930c8f // indirect
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
)
