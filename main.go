package main

import (
	_ "image/png"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

var dt float32
var player *Player = &Player{}
var scr *ebiten.Image

var lastTime time.Time

func update(screen *ebiten.Image) error {
	scr = screen

	dt = float32(time.Now().Sub(lastTime).Seconds())
	lastTime = time.Now()

	player.Update()

	ebitenutil.DebugPrint(screen, "akedah v0.0.0")

	return nil
}

func main() {
	player.Init()
	ebiten.SetMaxTPS(60)

	lastTime = time.Now()
	if err := ebiten.Run(update, 320, 320, 1, "akedah"); err != nil {
		panic(err)
	}
}
