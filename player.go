package main

import (
	"github.com/hajimehoshi/ebiten"
	"github.com/ungerik/go3d/vec2"
)

type Player struct {
	Position   *vec2.T
	Velocity   *vec2.T
	MoveVector *vec2.T

	Friction float32
	Speed    float32

	Image *ebiten.Image
}

func (p *Player) Init() {
	player.Friction = 0.5
	player.Speed = 2

	img, err := loadPicture("assets/player/player_down.png")
	if err != nil {
		panic(err)
	}

	p.Image, err = ebiten.NewImageFromImage(img, ebiten.FilterNearest)
	if err != nil {
		panic(err)
	}

	pos := vec2.Zero
	vel := vec2.Zero
	mov := vec2.Zero

	p.Position = &pos
	p.Velocity = &vel
	p.MoveVector = &mov

}

func (p *Player) Update() {
	player.Friction = 0.5
	player.Speed = 50
	kr := ebiten.IsKeyPressed(ebiten.KeyD)
	kl := ebiten.IsKeyPressed(ebiten.KeyA)
	ku := ebiten.IsKeyPressed(ebiten.KeyW)
	kd := ebiten.IsKeyPressed(ebiten.KeyS)

	if kr {
		p.MoveVector[0] = 1
	} else if kl {
		p.MoveVector[0] = -1
	} else {
		p.MoveVector[0] = 0
	}

	if kd {
		p.MoveVector[1] = 1
	} else if ku {
		p.MoveVector[1] = -1
	} else {
		p.MoveVector[1] = 0
	}

	uv := p.MoveVector.Normalized()
	if uv[0] != 0 || uv[1] != 0 {
		p.Velocity = p.Velocity.Add(uv.Scale(p.Speed * dt))
	}

	p.Position = p.Position.Add(p.Velocity)
	p.Velocity[0] *= p.Friction
	p.Velocity[1] *= p.Friction

	opts := &ebiten.DrawImageOptions{}
	opts.GeoM.Translate(float64(p.Position[0]), float64(p.Position[1]))

	scr.DrawImage(p.Image, opts)

	return
}
